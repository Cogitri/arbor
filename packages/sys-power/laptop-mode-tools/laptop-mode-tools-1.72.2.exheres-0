# Copyright 2008, 2009 Mike Kelly <pioto@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=rickysarraf tag=${PV} ] systemd-service udev-rules

SUMMARY="Laptop power saving package for Linux systems"
DESCRIPTION="
Laptop mode is a kernel \"mode\" that allows you to extend the battery
life of your laptop. It does this by making disk write activity
\"bursty\", so that only reads of uncached data result in a disk spinup.
It causes a significant improvement in battery life (for usage patterns
that allow it).
"

BUGS_TO="pioto@exherbo.org"
REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run:
        sys-apps/iproute2 [[ note = [ for shutting down ethernet devices ] ]]
        sys-power/acpid
"

src_compile() { :; }

src_install() {
    edo sed \
        -e 's:DESTDIR/usr/sbin:DESTDIR/usr/$SBIN_D:g' \
        -i install.sh

    DESTDIR="${IMAGE}" ACPI="force" MAN_D="/usr/share/man" LIB_D="lib" SBIN_D="$(exhost --target)/bin" \
        SYSTEMD_UNIT_D="${SYSTEMDSYSTEMUNITDIR}" TMPFILES_D="${SYSTEMDTMPFILESDIR}" \
        ULIB_D="/usr/$(exhost --target)/lib" UDEV_D="${UDEVDIR}" INIT_D="none" SYSTEMD="yes" \
        edo ./install.sh

    keepdir /etc/laptop-mode/{batt,lm-ac,nolm-ac}-{start,stop} /etc/laptop-mode/modules
}

