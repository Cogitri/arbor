[binaries]
c = 'x86_64-pc-linux-gnu-cc'
cpp = 'x86_64-pc-linux-gnu-c++'
ar = 'x86_64-pc-linux-gnu-ar'
strip = 'x86_64-pc-linux-gnu-strip'
pkgconfig = 'x86_64-pc-linux-gnu-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'x86_64'
cpu = 'x86_64'
endian = 'little'
