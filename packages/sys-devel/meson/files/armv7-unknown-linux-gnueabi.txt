[binaries]
c = 'armv7-unknown-linux-gnueabi-cc'
cpp = 'armv7-unknown-linux-gnueabi-c++'
ar = 'armv7-unknown-linux-gnueabi-ar'
strip = 'armv7-unknown-linux-gnueabi-strip'
pkgconfig = 'armv7-unknown-linux-gnueabi-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = 'armv7'
endian = 'little'
